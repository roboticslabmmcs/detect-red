vid = videoinput('winvideo', 1, 'MJPG_1280x720');
s = serial('COM10');
set(s,'BaudRate',19200);
fopen(s);
set(vid,'FramesPerTrigger',Inf);
set(vid,'ReturnedColorspace','rgb')
vid.FrameGrabInterval=5;

w=1280;
h=720;
move=1;
FAL=uint64(1000);
command_servo_b = 'CENTER';
command_thr_b = 'L50';
speed = 'L75';
speed_0 = 'L50';
%fprintf(s,'RUN');
start(vid);
current_frames=0;

while(move)
    while(vid.FramesAcquired<=FAL)
    
        if current_frames > 100
            current_frames=0;
            flushdata(vid,'all');
        end
        
        data=getsnapshot(vid);
        diff_im=imsubtract(data(:,:,1),rgb2gray(data));
        
        diff_im=medfilt2(diff_im,[3,3]);
        
        diff_im=im2bw(diff_im,0.18);
        
        diff_im=bwareaopen(diff_im,300);
        imshow(data);
        bw=bwlabel(diff_im,8);
        stats=regionprops(bw,'BoundingBox','Centroid'); %#ok<MRPBW>
        
    
        hold on
        bc = [];
        bb = [];
        for object=1:length(stats)
            bb=stats(object).BoundingBox;
            bc=stats(object).Centroid;
        
            rectangle('Position',bb,'EdgeColor','r','LineWidth',2)
            plot(bc(1),bc(2),'-m+')
        
        end
    
        hold off
    
        border_xl = w*0.35;
        border_xr = w*0.65;
        if ~(isempty(bc))
            if bc(1) < border_xl
                command_servo = 'LEFT';
                command_thr = speed;
            else
                if bc(1) > border_xr
                    command_servo = 'RIGHT';
                    command_thr = speed;
                else
                    command_servo = 'CENTER';
                    command_thr = speed;
                end
            end
        else
            command_servo = 'CENTER';
            command_thr = speed_0;
        end
        if ~strcmp(command_servo, command_servo_b)
            fprintf(s,command_servo);
        end
        if ~strcmp(command_thr, command_thr_b)
            fprintf(s,command_thr);
        end
        command_servo_b = command_servo;
        command_thr_b = command_thr;
        current_frames=current_frames+1;
    end
    
    if ~strcmp(command_servo, 'CENTER')
        command_servo = 'CENTER';
        fprintf(s,command_servo);
    end
    if ~strcmp(command_thr, speed_0)
        command_thr = speed_0;
        fprintf(s,command_thr);
    end
    command_servo_b = command_servo;
    command_thr_b = command_thr;
    
    text= input('����������? [y|n] ','s');
    if text=='y'
        move=1;
        AF = uint64(input('������� ����� ����������� �������������� ������: '));
        FAL=vid.FramesAcquired+AF;
    else
        move=0;
    end
end

command_servo = 'CENTER';
command_thr = speed_0;
fprintf(s,command_servo);
fprintf(s,command_thr);

stop(vid);
flushdata(vid);
fclose(s);

clear all;